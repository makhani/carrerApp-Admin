var express = require('express');
var router = express.Router();
var careerController = require('../controllers/careerController');

router.get('/careers',careerController.getCareers);
router.get('/career/:id',careerController.getCareer);
router.post('/career',careerController.addCareer);
router.put('/career/:id',careerController.editCareer);
router.delete('/career/:id',careerController.deleteCareer);

module.exports = router;
