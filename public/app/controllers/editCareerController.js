(function () {
	'use strict';
	var app = angular.module('careerApp');

	app.controller('editCareerController', function ($scope, $rootScope, $localStorage, $state, $stateParams, toastService, dashboardService, prompt, $timeout) {

		// Private Members
		$rootScope.title = 'Edit Career';

		$scope.career = {};

		//Data Members
		$rootScope.menuClass = 'editCareer';

		// Member functions
		$scope.editCareer = editCareer;
		activate();

		function activate() {
			$localStorage.careerId = ($stateParams.careerId && Object.keys($stateParams.careerId).length>0) ? $stateParams.careerId : $localStorage.careerId;
			dashboardService.getCareer($localStorage.careerId)
		    .then(function(data) {
		      if (data.success) {
		        $scope.career = data.data;
		      } else {
		        $state.go('dashboard');
		      }
		    }).catch(function(err) {
		        $state.go('dashboard');
		    });
		}

		function editCareer(){
			dashboardService.editCareer($localStorage.careerId, $scope.career).then(function(result) {
				toastService.message('success', 'Profession edited successfully');
				$timeout(function timeout() {
		    		$state.go('dashboard');
		      	},1000);
		    }).catch(function (err) {
		        err = err.error && err.error.error ? err.error.error : 'Sorry something went wrong';
		        toastService.message('danger', err);
		    });
		}
	});
})();
