(function(){
	'use strict';
	var app = angular.module('careerApp');

	app.controller('dashboardController',function($scope, $rootScope, $localStorage, $state, dashboardService, toastService, prompt){

		// Private Members
	    $rootScope.title = 'Dashboard';

	    //Data Members
	    $rootScope.menuClass = 'dashboard';

	    //Scope Functions
	    $scope.deleteCareer = deleteCareer;

	    activate();

	    function activate(){
	    	fetchCareers();
	    }
	    function fetchCareers() {
	    	dashboardService.getCareers().then(function(result) {
	    		$scope.careers = result.data;
		    }).catch(function (err) {
		        err = err.error && err.error.error ? err.error.error : 'Sorry something went wrong';
		        toastService.message('danger', err);
		    });
	    }

	    function deleteCareer(careerObj) {
	    	prompt({
		        title: 'Careers',
		        message: 'Are you sure you want to delete this career ?',
		        buttons:[{label:'Yes', primary: true}, {label:'No', cancel: true}]
		    }).then(function Success(result) {
		        if (result) {
		          	dashboardService.deleteCareer(careerObj.careerObj._id).then(function(result) {
			    		careerObj.deleteStatus = true;
				    }).catch(function (err) {
				        err = err.error && err.error.error ? err.error.error : 'Sorry something went wrong';
				        toastService.message('danger', err);
		    		});
		        }
      		});
	    }
	});
})();