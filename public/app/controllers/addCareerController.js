(function () {
	'use strict';
	var app = angular.module('careerApp');

	app.controller('addCareerController', function ($scope, $rootScope, $localStorage, $state, $stateParams, toastService, dashboardService, prompt, $timeout) {

		// Private Members
		function isNumber(evt) {
			alert("called");
    	return true;
    	}
		$rootScope.title = 'Add Career';

		$scope.career = {};

		//Data Members
		$rootScope.menuClass = 'addCareer';

		// Functions 
		$scope.addCareer = addCareer;

		function addCareer(){
			dashboardService.addCareer($scope.career).then(function(result) {
	    		toastService.message('success', 'Profession added successfully');
	    		$timeout(function timeout() {
		    		$state.go('dashboard');
		      	},1000);
		    }).catch(function (err) {
		        err = err.error && err.error.error ? err.error.error : 'Sorry something went wrong';
		        toastService.message('danger', err);
		    });
		}
	});
})();
