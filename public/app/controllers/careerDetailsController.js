(function () {
	'use strict';
	var app = angular.module('careerApp');

	app.controller('careerDetailsController', function ($scope, $rootScope, $localStorage, $state, $stateParams, toastService, prompt, dashboardService) {

		// Private Members
		$rootScope.title = 'Career Details';

		$scope.careerDetails = {};
		$scope.slides = [];
		//Data Members
		$rootScope.menuClass = 'careerDetails';

		activate();

		function activate() {
			$localStorage.careerObj = ($stateParams.careerObj && Object.keys($stateParams.careerObj).length>0) ? $stateParams.careerObj : $localStorage.careerObj;
			$scope.careerDetails = $localStorage.careerObj;
			$scope.slides = $scope.careerDetails.images.length && $scope.careerDetails.images[0]!='' ? $scope.careerDetails.images : [];
		}
	});
})();
