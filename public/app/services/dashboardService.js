(function() {
  'use strict';
  var app = angular.module('careerApp');

  app.factory('dashboardService',function($http,$q) {
    var factory = {
        getCareers : getCareers,
        getCareer : getCareer,
        addCareer : addCareer,
        editCareer : editCareer,
        deleteCareer : deleteCareer
      };
    return factory;

    //This function is used to get careers
    function getCareers() {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: 'api/careers',
      }).then(function(data) {
        deferred.resolve(data.data);
      }).catch(function(error) {
        deferred.reject(error.data);
      });
      return deferred.promise;
    }

    //This function is used to get single career
    function getCareer(careerId) {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: 'api/career/'+careerId,
      }).then(function(data) {
        deferred.resolve(data.data);
      }).catch(function(error) {
        deferred.reject(error.data);
      });
      return deferred.promise;
    }

    //This function is used to add career
    function addCareer(careerObj) {
      var deferred = $q.defer();
      console.log(careerObj);
      $http({
        method: 'POST',
        url: 'api/career',
        data: {careerObj:careerObj}
      }).then(function(data) {
        deferred.resolve(data.data);
      }).catch(function(error) {
        deferred.reject(error.data);
      });
      return deferred.promise;
    }

    //This function is used to edit career
    function editCareer(careerId, careerObj) {
      var deferred = $q.defer();
      $http({
        method: 'PUT',
        url: 'api/career/'+careerId,
        data: {careerObj:careerObj}
      }).then(function(data) {
        deferred.resolve(data.data);
      }).catch(function(error) {
        deferred.reject(error.data);
      });
      return deferred.promise;
    }

    //This function is used to delete career
    function deleteCareer(careerId) {
      var deferred = $q.defer();
      $http({
        method: 'DELETE',
        url: 'api/career/'+careerId,
      }).then(function(data) {
        deferred.resolve(data.data);
      }).catch(function(error) {
        deferred.reject(error.data);
      });
      return deferred.promise;
    }
  });
})();