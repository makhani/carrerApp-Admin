(function() {
	'use strict';
	angular.module('careerApp')
	  .service('toastService', function (ngToast) {
	  var toastObj =  {
	    message: function(type, msg) {
	      var toastOptions = {
	        className:type,
	        content:msg,
	        dismissButton:true
	      };
	      ngToast.create(toastOptions);
	    }
	  };
	  return toastObj;
	});
})();
