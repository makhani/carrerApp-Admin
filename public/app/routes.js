(function () {
  'use strict';

  var app = angular.module('careerApp');

  app.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, ngToastProvider, ngIntlTelInputProvider, $qProvider) {
    
    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
      .state('app', {
        url: '',
        templateUrl: 'app/views/layout.html',
        abstract:true,
        controller:function ($cookies,$localStorage,$state,$scope,$rootScope,toastService) {
          /*$scope.logout = function() {
            authenticationService.logout().then(function() {
              //$cookies.remove('authToken');
              //$localStorage.$reset();
              //$state.go('login');
            }).catch(function(err) {
              err = err.error && err.error.error ? err.error.error : 'Sorry something went wrong';
              toastService.message('error', err);
            });
          };*/
        }
      })
      .state('dashboard', {
        parent:'app',
        url: '/dashboard',
        views: {
          'content' : {
            templateUrl: 'app/views/dashboard.html',
            controller:'dashboardController'
          }
        }
      })
      .state('viewCareer', {
        parent:'app',
        url: '/career/view',
        params: {careerObj:{}},
        views: {
          'content' : {
            templateUrl: 'app/views/careerDetails.html',
            controller:'careerDetailsController'
          }
        }
      })
      .state('addCareer', {
        parent:'app',
        url: '/career/add',
        params: {careerObj:{}},
        views: {
          'content' : {
            templateUrl: 'app/views/addCareer.html',
            controller:'addCareerController'
          }
        }
      })
      .state('editCareer', {
        parent:'app',
        url: '/career/edit',
        params: {careerId:{}},
        views: {
          'content' : {
            templateUrl: 'app/views/editCareer.html',
            controller:'editCareerController'
          }
        }
      });

    // Toast message configuration
    ngToastProvider.configure({
      horizontalPosition:'center',
      verticalPosition:'top',
      maxNumber:1,
      newestOnTop:true
    });

    $qProvider.errorOnUnhandledRejections(false);

    // api interceptor
    $httpProvider.interceptors.push('authInterceptor');

  }).factory('authInterceptor', function ($rootScope, $q, $location, $cookies, $localStorage) {
    var apiURL = window.location.origin;

    return {
      request: function (config) {
        if (config.url && config.url.indexOf('api/') !== -1) {
          config.headers.authtoken = $cookies.get('authToken');
          config.url = config.url.substr(config.url.indexOf('/'),config.url.length);
          config.url = apiURL + config.url;
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if (response.status === 401) {
          $location.path('/dashboard');
        }
        return $q.reject(response);
      }
    };
  });

  // This function is used to resolve career
  /*function getCareer(dashboardService, $localStorage, $rootScope, $state, $stateParams, appConstants, $q,$timeout) {
    var deferred = $q.defer();
    //deferred.resolve([]);
    console.log($state, $stateParams);
    dashboardService.getCareer()
    .then(function success(data) {
      if (data.success) {
        deferred.resolve(data);
      } else {
        $timeout(function timeout() {
          $state.go('dashboard');
        },0);
      }
    }).catch(function error() {
      $timeout(function timeout() {
        $state.go('dashboard');
      },0);
    });
    return deferred.promise;
  }*/
})();
