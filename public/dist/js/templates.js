angular.module('careerappTemplates', ['../public/app/views/addCareer.html', '../public/app/views/careerDetails.html', '../public/app/views/dashboard.html', '../public/app/views/editCareer.html', '../public/app/views/layout.html']);

angular.module("../public/app/views/addCareer.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../public/app/views/addCareer.html",
    "<div class='container-fluid dashboardContainer'>\n" +
    "    <div class='row'>\n" +
    "        <div class='col-md-10 col-md-offset-3'>\n" +
    "            <form name=\"careerForm\" novalidate>\n" +
    "                <!-- NAME -->\n" +
    "                 <legend>Add Profession</legend>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\" >Name<sup>*</sup> : </label>\n" +
    "                    <textarea name=\"name\" onkeypress='return isNumber(event)'  class=\"col-md-10 textArea\"  ng-model=\"career.name\" required></textarea>\n" +
    "                    <p ng-show=\"careerForm.name.$invalid && !careerForm.name.$pristine\" class=\"help-block\">Profession name is required.</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Description<sup>*</sup> : </label>\n" +
    "                    <textarea name=\"description\" onkeypress=\"return isNumber(event)\" type=\"number\" class=\"col-md-10 textArea\" ng-model=\"career.description\" required></textarea>\n" +
    "                    <p ng-show=\"careerForm.description.$invalid && !careerForm.description.$pristine\" class=\"help-block\">Profession description is required.</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Stream : </label>\n" +
    "                    <textarea name=\"stream \" type=\"number\" class=\"col-md-10 textArea\" ng-model=\"career.stream\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Type of Stream : </label>\n" +
    "                    <label><input type=\"radio\" class=\"checkBox\" name=\"typeOfStrem\" ng-model=\"career.typeOfStream\" value='technical'> Technical</label>\n" +
    "                    <label><input type=\"radio\" class=\"checkBox\" name=\"typeOfStrem\" ng-model=\"career.typeOfStream\" value='non-technical'> Non-technical</label>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Skills Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"skillsNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.skillsNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Education Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"educationNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.educationNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Knowledge Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"knowledgeNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.knowledgeNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Language Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"languageNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.languageNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Other Things Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"otherThingsNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.otherThingsNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Potential Training Providers (~ seperated) : </label>\n" +
    "                    <textarea name=\"potentialTrainingProviders\" class=\"col-md-10 textArea\" ng-model=\"career.potentialTrainingProviders\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-4 Label\">Min. Investment Needed : </label>\n" +
    "                    <textarea name=\"minInvestment\" class=\"col-md-4 textArea\" ng-model=\"career.minInvestment\"></textarea>\n" +
    "                    <label class=\"col-md-4 Label\">Max. Investment Needed : </label>\n" +
    "                    <textarea name=\"maxInvestment\" class=\"col-md-4 textArea\" ng-model=\"career.maxInvestment\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Other Investment Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"otherInvestment\" class=\"col-md-10 textArea\" ng-model=\"career.otherInvestment\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">How to invest (~ seperated) : </label>\n" +
    "                    <textarea name=\"investmentExplanation\" class=\"col-md-10 textArea\" ng-model=\"career.investmentExplanation\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Source Of Funds (~ seperated) : </label>\n" +
    "                    <textarea name=\"sourceOfFunds\" class=\"col-md-10 textArea\" ng-model=\"career.sourceOfFunds\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-3 Label\" type=\"number\" >Min. Income : </label>\n" +
    "                    <textarea name=\"minIncome\" class=\"col-md-5 textArea\" ng-model=\"career.minIncome\"></textarea>\n" +
    "                    <label class=\"col-md-3 Label\">Max. Income : </label>\n" +
    "                    <textarea name=\"maxIncome\" class=\"col-md-5 textArea\" ng-model=\"career.maxIncome\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">How to make income (~ seperated) : </label>\n" +
    "                    <textarea name=\"incomeExplanation\" class=\"col-md-10 textArea\" ng-model=\"career.incomeExplanation\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Type of work (~ seperated) : </label>\n" +
    "                    <textarea name=\"typeOfWork\" class=\"col-md-10 textArea\" ng-model=\"career.typeOfWork\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Working Hours : </label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"partTime\" ng-model=\"career.partTime\"> Part Time</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"fullTime\" ng-model=\"career.fullTime\"> Full Time</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"freeLancer\" ng-model=\"career.freeLancer\"> Free Lancer</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"workFromHome\" ng-model=\"career.workFromHome\"> Work From Home</label>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Where to start (~ seperated) : </label>\n" +
    "                    <textarea name=\"whereToStart\" class=\"col-md-10 textArea\" ng-model=\"career.whereToStart\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Future Demand (~ seperated) : </label>\n" +
    "                    <textarea name=\"futureDemand\" class=\"col-md-10 textArea\" ng-model=\"career.futureDemand\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Functional Experts (~ seperated) : </label>\n" +
    "                    <textarea name=\"functionalExperts\" class=\"col-md-10 textArea\" ng-model=\"career.functionalExperts\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-5 Label\">Min. Course Duration (in months) : </label>\n" +
    "                    <textarea name=\"minDuration\" class=\"col-md-3 textArea\" ng-model=\"career.minDuration\"></textarea>\n" +
    "                    <label class=\"col-md-5 Label\">Max. Course Duration (in months) : </label>\n" +
    "                    <textarea name=\"maxDuration\" class=\"col-md-3 textArea\" ng-model=\"career.maxDuration\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Hiring Companies (~ seperated) : </label>\n" +
    "                    <textarea name=\"hiringCompanies\" class=\"col-md-10 textArea\" ng-model=\"career.hiringCompanies\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Where to find jobs (~ seperated) : </label>\n" +
    "                    <textarea name=\"whereToFindJobs\" class=\"col-md-10 textArea\" ng-model=\"career.whereToFindJobs\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Risks Assoicated (~ seperated) : </label>\n" +
    "                    <textarea name=\"risksAssociated\" class=\"col-md-10 textArea\" ng-model=\"career.risksAssociated\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Expected Career Survival (~ seperated) : </label>\n" +
    "                    <textarea name=\"expectedCareerSurvival\" class=\"col-md-10 textArea\" ng-model=\"career.expectedCareerSurvival\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Next Steps To Grow (~ seperated) : </label>\n" +
    "                    <textarea name=\"nextStepsToGrow\" class=\"col-md-10 textArea\" ng-model=\"career.nextStepsToGrow\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Interested Areas (~ seperated) : </label>\n" +
    "                    <textarea name=\"interestAreas\" class=\"col-md-10 textArea\" ng-model=\"career.interestAreas\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Reviews By Experts (~ seperated) : </label>\n" +
    "                    <textarea name=\"reviewsByExperts\" class=\"col-md-10 textArea\" ng-model=\"career.reviewsByExperts\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Tips to Make Money (~ seperated) : </label>\n" +
    "                    <textarea name=\"tipsToMakeMoney\" class=\"col-md-10 textArea\" ng-model=\"career.tipsToMakeMoney\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Search Keywords (~ seperated) : </label>\n" +
    "                    <textarea name=\"searchKeywords\" class=\"col-md-10 textArea\" ng-model=\"career.searchKeywords\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <button type=\"submit\" class=\"col-md-offset-7 btn btn-primary\" ng-disabled=\"careerForm.$invalid\" ng-click='addCareer()'>Submit</button>\n" +
    "                    <input type=\"reset\" class=\"btn btn-secondary\" style=\"margin-left:15px\">\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../public/app/views/careerDetails.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../public/app/views/careerDetails.html",
    "<div class='container-fluid careerDetailsContainer'>\n" +
    "	<div class='detailsTitle'>\n" +
    "		Profession Details\n" +
    "	</div>\n" +
    "	<div class='row detailsRow'>\n" +
    "		<div class='col-lg-6 col-md-6 col-sm-16 col-xs-16'>\n" +
    "			<div class='detailsImg' ng-if=\"slides.length\">\n" +
    "				<img class='instImg' ng-src=\"../../assets/images/careers/{{slides[0]}}\" />\n" +
    "			</div>\n" +
    "			<div class='detailsImg' ng-if=\"!slides.length\">\n" +
    "				<div style='height:450px;width:450px;padding-top:200px;font-size:2rem;text-align:center;border:1px solid grey;'>No Image Available.</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='col-lg-10 col-md-10 col-sm-16 col-xs-16 details'>\n" +
    "			<div class='row'>\n" +
    "				<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Name :</div>\n" +
    "				<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'> {{careerDetails.name}} </div>\n" +
    "			</div>\n" +
    "			<div class='row' ng-if='careerDetails.description'>\n" +
    "				<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Description :</div>\n" +
    "				<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12' ng-bind-html=\"careerDetails.description | to_trusted\"></div>\n" +
    "			</div>\n" +
    "			<div class='row' ng-if=\"careerDetails.preRequisite && careerDetails.preRequisite.skillsNeeded.length\">\n" +
    "				<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Skills Needed :</div>\n" +
    "				<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "					<div ng-repeat='skill in careerDetails.preRequisite.skillsNeeded track by $index'>\n" +
    "						{{$index+1}})\n" +
    "						<span ng-bind-html=\"skill | to_trusted\"></span> \n" +
    "					</div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.preRequisite && careerDetails.preRequisite.educationNeeded.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Education Needed :</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='education in careerDetails.preRequisite.educationNeeded track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"education | to_trusted\"></span> \n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.preRequisite && careerDetails.preRequisite.knowledgeNeeded.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Knowledge Needed :</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='knowledge in careerDetails.preRequisite.knowledgeNeeded track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"knowledge | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.preRequisite && careerDetails.preRequisite.languageNeeded.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Languages Needed :</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='language in careerDetails.preRequisite.languageNeeded track by $index'>\n" +
    "					{{$index+1}}) {{language}}\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.preRequisite && careerDetails.preRequisite.others.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Other Things Needed :</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='other in careerDetails.preRequisite.others track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"other | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.potentialTrainingProviders.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Potential Training Providers:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='traningProvider in careerDetails.potentialTrainingProviders track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"traningProvider | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.investmentNeeded && careerDetails.investmentNeeded.minInvestment && careerDetails.investmentNeeded.maxInvestment\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Investment Needed:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				Rs. {{careerDetails.investmentNeeded.minInvestment}} - Rs. {{careerDetails.investmentNeeded.maxInvestment}}\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.investmentNeeded && careerDetails.investmentNeeded.otherInvestment.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Other Investment:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='investment in careerDetails.investmentNeeded.otherInvestment track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"investment | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.investmentNeeded && careerDetails.investmentNeeded.investmentExplanation.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>How to invest:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='investment in careerDetails.investmentNeeded.investmentExplanation track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"investment | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.sourceOfFunds.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Source Of Funds:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='fund in careerDetails.sourceOfFunds track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"fund| to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.income && careerDetails.income.minIncome && careerDetails.income.maxIncome\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Income:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				Rs. {{careerDetails.income.minIncome}} - Rs. {{careerDetails.income.maxIncome}}\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.income && careerDetails.income.incomeExplanation.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>How to make income:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='income in careerDetails.income.incomeExplanation track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"income | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.typeOfWork.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Type of work:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='work in careerDetails.typeOfWork track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"work | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.workingHours.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Working Hours:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<span ng-if='careerDetails.workingHours.partTime'>Part Time,</span>\n" +
    "				<span ng-if='careerDetails.workingHours.fullTime'>Full Time,</span>\n" +
    "				<span ng-if='careerDetails.workingHours.freeLancer'>Free Lancer,</span>\n" +
    "				<span ng-if='careerDetails.workingHours.workFromHome'>Work From Home,</span>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.whereToStart.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Where to start:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='step in careerDetails.whereToStart track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"step | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.futureDemand.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Future Demand:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='demand in careerDetails.futureDemand track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"demand | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.functionalExperts.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Functional Experts:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='expert in careerDetails.functionalExperts track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\" expert | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.careerDuration && careerDetails.careerDuration.minDuration && careerDetails.careerDuration.maxDuration\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Course Duration:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				{{careerDetails.careerDuration.minDuration}} {{careerDetails.careerDuration.minDuration/12\n" +
    "				< 1 ? 'Months' : 'Years'}}\n" +
    "				- {{careerDetails.careerDuration.maxDuration}} {{careerDetails.careerDuration.maxDuration/12 < 1 ? 'Months' :\n" +
    "			'Years'}} </div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.hiringCompanies.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Hiring Companies:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='company in careerDetails.hiringCompanies track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"company | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.whereToFindJobs.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Where to find jobs:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='job in careerDetails.whereToFindJobs track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"job | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.risksAssociated.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Risks Assoicated:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='risk in careerDetails.risksAssociated track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"risk | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.expectedCareerSurvival.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Expected Career Survival:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='careerSurvival in careerDetails.expectedCareerSurvival track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"careerSurvival | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.nextStepsToGrow.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Next Steps To Grow:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='step in careerDetails.nextStepsToGrow track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"step | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.interestAreas.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Interested Areas:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='interest in careerDetails.interestAreas track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"interest | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.reviewsByExperts.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Reviews By Experts:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='review in careerDetails.reviewsByExperts track by $index'>\n" +
    "					{{$index+1}}) \n" +
    "					<span ng-bind-html=\"review | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div class='row' ng-if=\"careerDetails.tipsToMakeMoney.length\">\n" +
    "			<div class='detailsLabel col-lg-4 col-md-4 col-sm-4 col-xs-4'>Tips to Make Money:</div>\n" +
    "			<div class='detailsValue col-lg-12 col-md-12 col-sm-12 col-xs-12'>\n" +
    "				<div ng-repeat='tip in careerDetails.tipsToMakeMoney track by $index'>\n" +
    "					{{$index+1}})\n" +
    "					<span ng-bind-html=\"tip | to_trusted\"></span>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "</div>");
}]);

angular.module("../public/app/views/dashboard.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../public/app/views/dashboard.html",
    "<div class='container-fluid dashboardContainer'>\n" +
    "  <div class='row searchResultsDiv'>\n" +
    "    <div class='col-md-14 col-md-offset-1'>\n" +
    "        <table class='table table-hover'>\n" +
    "          <th>Index</th>\n" +
    "          <th>Name</th>\n" +
    "          <th>Description</th>\n" +
    "          <th></th>\n" +
    "          <th></th>\n" +
    "          <th></th>\n" +
    "\n" +
    "          <tr ng-repeat='career in careers'>\n" +
    "            <td>{{$index+1}}.</td>\n" +
    "            <td>{{career.name}}</td>\n" +
    "            <td>{{career.description}}</td>\n" +
    "            <td>\n" +
    "              <button type=\"button\" class=\"btn btn-sm btn-default\" ui-sref=\"viewCareer({careerObj:career})\" style=\"margin-top: 10px;margin-bottom: 10px;\">View</button>\n" +
    "            </td>\n" +
    "            <td>\n" +
    "              <button type=\"button\" class=\"btn btn-sm btn-default\" ui-sref=\"editCareer({careerId:career._id})\" style=\"margin-top: 10px;margin-bottom: 10px;\">Edit</button>\n" +
    "            </td>\n" +
    "            <td>\n" +
    "              <button type=\"button\" class=\"btn btn-sm btn-default\" ng-click=\"deleteCareer({careerObj:career})\" style=\"margin-top: 10px;margin-bottom: 10px;\">Delete</button>\n" +
    "            </td>\n" +
    "          </tr>\n" +
    "        </table>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("../public/app/views/editCareer.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../public/app/views/editCareer.html",
    "<div class='container-fluid dashboardContainer'>\n" +
    "    <div class='row'>\n" +
    "        <div class='col-md-10 col-md-offset-3'>\n" +
    "            <form name=\"careerForm\" novalidate>\n" +
    "                <!-- NAME -->\n" +
    "                 <legend>Add Profession</legend>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Name<sup>*</sup> : </label>\n" +
    "                    <textarea name=\"name\" class=\"col-md-10 textArea\" ng-model=\"career.name\" required></textarea>\n" +
    "                    <p ng-show=\"careerForm.name.$invalid && !careerForm.name.$pristine\" class=\"help-block\">Profession name is required.</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Description<sup>*</sup> : </label>\n" +
    "                    <textarea name=\"description\" class=\"col-md-10 textArea\" ng-model=\"career.description\" required></textarea>\n" +
    "                    <p ng-show=\"careerForm.description.$invalid && !careerForm.description.$pristine\" class=\"help-block\">Profession description is required.</p>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Stream : </label>\n" +
    "                    <textarea name=\"stream\" class=\"col-md-10 textArea\" ng-model=\"career.stream\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Type of Stream : </label>\n" +
    "                    <label><input type=\"radio\" class=\"checkBox\" name=\"typeOfStrem\" ng-model=\"career.typeOfStream\" value='technical'> Technical</label>\n" +
    "                    <label><input type=\"radio\" class=\"checkBox\" name=\"typeOfStrem\" ng-model=\"career.typeOfStream\" value='non-technical'> Non-technical</label>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Skills Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"skillsNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.skillsNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Education Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"educationNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.educationNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Knowledge Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"knowledgeNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.knowledgeNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Language Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"languagesNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.languagesNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Other Things Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"otherThingsNeeded\" class=\"col-md-10 textArea\" ng-model=\"career.otherThingsNeeded\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Potential Training Providers (~ seperated) : </label>\n" +
    "                    <textarea name=\"potentialTrainingProviders\" class=\"col-md-10 textArea\" ng-model=\"career.potentialTrainingProviders\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-4 Label\">Min. Investment Needed : </label>\n" +
    "                    <textarea name=\"minInvestment\" class=\"col-md-4 textArea\" ng-model=\"career.minInvestment\"></textarea>\n" +
    "                    <label class=\"col-md-4 Label\">Max. Investment Needed : </label>\n" +
    "                    <textarea name=\"maxInvestment\" class=\"col-md-4 textArea\" ng-model=\"career.maxInvestment\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Other Investment Needed (~ seperated) : </label>\n" +
    "                    <textarea name=\"otherInvestment\" class=\"col-md-10 textArea\" ng-model=\"career.otherInvestment\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">How to invest (~ seperated) : </label>\n" +
    "                    <textarea name=\"investmentExplanation\" class=\"col-md-10 textArea\" ng-model=\"career.investmentExplanation\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Source Of Funds (~ seperated) : </label>\n" +
    "                    <textarea name=\"sourceOfFunds\" class=\"col-md-10 textArea\" ng-model=\"career.sourceOfFunds\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-3 Label\">Min. Income : </label>\n" +
    "                    <textarea name=\"minIncome\" class=\"col-md-5 textArea\" ng-model=\"career.minIncome\"></textarea>\n" +
    "                    <label class=\"col-md-3 Label\">Max. Income : </label>\n" +
    "                    <textarea name=\"maxIncome\" class=\"col-md-5 textArea\" ng-model=\"career.maxIncome\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">How to make income (~ seperated) : </label>\n" +
    "                    <textarea name=\"incomeExplanation\" class=\"col-md-10 textArea\" ng-model=\"career.incomeExplanation\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Type of work (~ seperated) : </label>\n" +
    "                    <textarea name=\"typeOfWork\" class=\"col-md-10 textArea\" ng-model=\"career.typeOfWork\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Working Hours : </label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"partTime\" ng-model=\"career.partTime\"> Part Time</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"fullTime\" ng-model=\"career.fullTime\"> Full Time</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"freeLancer\" ng-model=\"career.freeLancer\"> Free Lancer</label>\n" +
    "                    <label><input type=\"checkbox\" class=\"checkBox\" name=\"workFromHome\" ng-model=\"career.workFromHome\"> Work From Home</label>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Where to start (~ seperated) : </label>\n" +
    "                    <textarea name=\"whereToStart\" class=\"col-md-10 textArea\" ng-model=\"career.whereToStart\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Future Demand (~ seperated) : </label>\n" +
    "                    <textarea name=\"futureDemand\" class=\"col-md-10 textArea\" ng-model=\"career.futureDemand\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Functional Experts (~ seperated) : </label>\n" +
    "                    <textarea name=\"functionalExperts\" class=\"col-md-10 textArea\" ng-model=\"career.functionalExperts\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-5 Label\">Min. Course Duration (in months) : </label>\n" +
    "                    <textarea name=\"minDuration\" class=\"col-md-3 textArea\" ng-model=\"career.minDuration\"></textarea>\n" +
    "                    <label class=\"col-md-5 Label\">Max. Course Duration (in months) : </label>\n" +
    "                    <textarea name=\"maxDuration\" class=\"col-md-3 textArea\" ng-model=\"career.maxDuration\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Hiring Companies (~ seperated) : </label>\n" +
    "                    <textarea name=\"hiringCompanies\" class=\"col-md-10 textArea\" ng-model=\"career.hiringCompanies\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Where to find jobs (~ seperated) : </label>\n" +
    "                    <textarea name=\"whereToFindJobs\" class=\"col-md-10 textArea\" ng-model=\"career.whereToFindJobs\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Risks Assoicated (~ seperated) : </label>\n" +
    "                    <textarea name=\"risksAssociated\" class=\"col-md-10 textArea\" ng-model=\"career.risksAssociated\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Expected Career Survival (~ seperated) : </label>\n" +
    "                    <textarea name=\"expectedCareerSurvival\" class=\"col-md-10 textArea\" ng-model=\"career.expectedCareerSurvival\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Next Steps To Grow (~ seperated) : </label>\n" +
    "                    <textarea name=\"nextStepsToGrow\" class=\"col-md-10 textArea\" ng-model=\"career.nextStepsToGrow\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Interested Areas (~ seperated) : </label>\n" +
    "                    <textarea name=\"interestAreas\" class=\"col-md-10 textArea\" ng-model=\"career.interestAreas\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Reviews By Experts (~ seperated) : </label>\n" +
    "                    <textarea name=\"reviewsByExperts\" class=\"col-md-10 textArea\" ng-model=\"career.reviewsByExperts\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Tips to Make Money (~ seperated) : </label>\n" +
    "                    <textarea name=\"tipsToMakeMoney\" class=\"col-md-10 textArea\" ng-model=\"career.tipsToMakeMoney\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-md-6 Label\">Search Keywords (~ seperated) : </label>\n" +
    "                    <textarea name=\"searchKeywords\" class=\"col-md-10 textArea\" ng-model=\"career.searchKeywords\"></textarea>\n" +
    "                </div>\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <button type=\"submit\" class=\"col-md-offset-7 btn btn-primary\" ng-disabled=\"careerForm.$invalid\" ng-click='editCareer()'>Submit</button>\n" +
    "                    <input type=\"reset\" class=\"btn btn-secondary\" style=\"margin-left:15px\">\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../public/app/views/layout.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../public/app/views/layout.html",
    "<!-- Fixed navbar -->\n" +
    "      <nav class=\"navbar navbar-default navbar-fixed-top navpanel headerNavbar\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "          <div class=\"navbar-header\">\n" +
    "            <a class=\"navbar-left navlogo\" ui-sref=\"dashboard\">Professions</a>\n" +
    "\n" +
    "          </div>\n" +
    "          <a class=\"navbar-right navlogo\" style='margin-right:15px;' ui-sref=\"addCareer\">Add Profession</a>\n" +
    "        </div>\n" +
    "      </nav>\n" +
    "\n" +
    "      <!-- <div class=\"container-fluid\" style=\"height: 100%\">\n" +
    "        <div class=\"row\" style=\"height: 100%\">\n" +
    "          <div class=\"col-lg-16 col-md-16 col-sm-16 col-xs-16\" style=\"height: 100%\"> -->\n" +
    "            <!-- your page content -->\n" +
    "                <div ui-view=\"content\" style=\"height: 100%\"></div>\n" +
    "          <!-- </div>\n" +
    "        </div>\n" +
    "      </div> -->\n" +
    "\n" +
    "      <!-- <div class='footer'>\n" +
    "        &copy; AKESI 2017\n" +
    "      </div> -->\n" +
    "\n" +
    "");
}]);
