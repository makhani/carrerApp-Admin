// grab the things we need
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// Note: Duration is in months and money is in thousands 

// create a schema
var career = new Schema({
  name: String,
  description: String,
  stream: String,
  typeOfStream: {
    type: String,
    enum: ['technical', 'non-techincal'],
    default: 'non-techincal'
  },
  preRequisite: {
    skillsNeeded: [{type: String}],
    knowledgeNeeded: [{type: String}],
    educationNeeded: [{type: String}],
    languagesNeeded: [{type: String}],
    others: [{type: String}]
  },
  potentialTrainingProviders: [{type: String}],
  investmentNeeded: {
    minInvestment: Number,
    maxInvestment: Number,
    otherInvestment: [{type: String}],
    investmentExplanation: [{type: String}]
  },
  sourceOfFunds: [{type: String}],
  typeOfWork: [{type: String}],
  whereToStart: [{type: String}],
  futureDemand: [{type: String}],
  functionalExperts: [{type: String}],
  careerDuration: {
    minDuration: Number,
    maxDuration: Number,
    durationExplanation: [{type: String}]
  },
  hiringCompanies: [{type: String}],
  whereToFindJobs: [{type: String}],
  income: {
    minIncome: Number,
    maxIncome: Number,
    incomeExplanation: [{type: String}]
  },
  risksAssociated: [{type: String}],
  expectedCareerSurvival: [{type: String}],
  nextStepsToGrow: [{type: String}],
  interestAreas: [{type: String}],
  reviewsByExperts: [{type: String}],
  workingHours: {
    partTime: {type: Boolean, default: false},
    fullTime: {type: Boolean, default: false},
    freeLancer: {type: Boolean, default: false},
    workFromHome: {type: Boolean, default: false},
    others: [{type: String}]
  },
  tipsToMakeMoney: [{type: String}],
  searchKeywords: [{type: String}],
  images: [{type: String}]
},
{timestamps: {
  createdAt: 'createdDate',
  updatedAt: 'updatedDate'
}});
// the schema is useless so far
// we need to create a model using it
var Career = mongoose.model('Career', career);

// make this available to our users in our Node applications
module.exports = Career;
