// grab the things we need
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;


// create a schema
var institute = new Schema({
  name: String,
  description: String,
  location: [{type: String}],
  fees: Number,
  placements: [{type: String}],
  ranking: Number,
  images: [{type: String}],
  website: String,
  minSalaryOffers: Number,
  maxSalaryOffers: Number,
  courseOffers: [
  	{
  		type: ObjectId,
		  ref: 'Course'
  	}
  ]
},
{timestamps: {
  createdAt: 'createdDate',
  updatedAt: 'updatedDate'
}});
// the schema is useless so far
// we need to create a model using it
var Institute = mongoose.model('Institute', institute);

// make this available to our users in our Node applications
module.exports = Institute;
