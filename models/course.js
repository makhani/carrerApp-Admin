// grab the things we need
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;


// create a schema
var course = new Schema({
  courseName: String, // 'Bachelore Of Engineering','Master Of Engineering'
  courseType: String, // 'Degree','Diploma'
  stream: String, // 'Engineering','Commerce','Arts','Science'
  subStream: String, // 'Computer Engineering','Mechanical Engineering'
  durationInYears: Number,
  minSalaryOffers: Number,
  maxSalaryOffers: Number
},
{timestamps: {
  createdAt: 'createdDate',
  updatedAt: 'updatedDate'
}});
// the schema is useless so far
// we need to create a model using it
var Course = mongoose.model('Course', course);

// make this available to our users in our Node applications
module.exports = Course;
