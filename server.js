// server.js

// modules =================================================
var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var cors = require('cors');
var device = require('express-device');
require('dotenv').config();

// var config = require('./config')

//global.logger = require('./services/log');


// configuration ===========================================

var dbUrl = process.env.DBCONNECTION;
// set our port
var port = process.env.PORT || 8080; 

// connect to our mongoDB database 
// (uncomment after you enter in your own credentials in config/db.js)
mongoose.connect(dbUrl, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + dbUrl + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + dbUrl);
    // console.log();
    // console.log('************** Please wait migrations started **************');
    // console.log();
    //require('./seeds/mongoSeed.js')('users');
  }
});

/*if (config.env === 'prod') {
  var processor = require('./processor');
  processor.init().catch(function (error) {
    console.log(error.stack);
    throw error;
  });
}*/

// get all data/stuff of the body (POST) parameters
// parse application/json 
app.use(bodyParser.json());
app.use(device.capture());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override')); 
app.use(cors());
// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public')); 

// routes ==================================================
var apiRoutes = require('./routes/api'); 
// console.log(routes)
app.get('/', function(req, res) {
  res.sendFile('../public/index.html'); // load our public/index.html file
});

app.use('/', apiRoutes);

// start app ===============================================
// startup our app at http://localhost:8080
app.listen(port);               

// shoutout to the user                     
console.log('Magic happens on port ' + port);

// expose app           
exports = module.exports = app; 