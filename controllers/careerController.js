// var Course = require('../models/course');
// var Institute = require('../models/institute');
var Career = require('../models/career');
var moment = require('moment');
var _ = require('lodash');
var co = require('co');
var Promise = require('bluebird');
var thunkify = require('thunkify');
var async = require('async');
require('dotenv').config();


exports.getCareers = function (req, res) {
	co(function* () {
		var careers = yield Career.find().exec();
		// console.log(careers)
		return res.status(200).json({ success: true, data: careers });
	}).catch(function (err) {
		console.log('error --- ', err);
		return res.status(500).json({ success: false, error: err });
	});
};

exports.getCareer = function (req, res) {
	co(function* () {
		console.log('inside getCareer ----------- ')
		var career = yield Career.findOne({_id:req.params.id}).exec();
		career = yield setCarrerObj(career);
		return res.status(200).json({ success: true, data: career });
	}).catch(function (err) {
		console.log('error --- ', err);
		return res.status(500).json({ success: false, error: err });
	});
};

exports.addCareer = function (req, res) {
	co(function* () {
		var career = yield getCarrerObj(req.body.careerObj);
		// console.log(career);
		yield Career.create(career);
		return res.status(200).json({success: true});
	}).catch(function (err) {
		console.log('error --- ', err);
		return res.status(500).json({ success: false, error: err });
	});
};

exports.editCareer = function (req, res) {
	co(function* () {
		var career = yield getCarrerObj(req.body.careerObj);
		yield Career.findOneAndUpdate({_id:req.params.id},career).exec();
		return res.status(200).json({success: true});
	}).catch(function (err) {
		console.log('error --- ', err);
		return res.status(500).json({ success: false, error: err });
	});
};

exports.deleteCareer = function (req, res) {
	co(function* () {
		console.log(req.params.id.toString())
		yield Career.remove({_id:req.params.id}).exec();
		return res.status(200).json({success: true});
	}).catch(function (err) {
		console.log('error --- ', err);
		return res.status(500).json({ success: false, error: err });
	});
};

function getCarrerObj(careerData){
	return new Promise(function(resolve, reject){
		co(function*(){
			var careerObj = {
				preRequisite: {},
				investmentNeeded: {},
				careerDuration: {},
				income: {},
				workingHours: {}
			};
			
			yield Promise.each(Object.keys(careerData), co.wrap(function*(value,key){
				switch(value) {
					case 'name':
						careerObj.name = careerData[value];
						break;
					case 'description':
						careerObj.description = careerData[value];
						break;
					case 'stream':
						careerObj.stream = careerData[value];
						break;
					case 'typeOfStream':
						careerObj.typeOfStream = careerData[value];
						break;
					case 'skillsNeeded':
						careerObj.preRequisite.skillsNeeded = careerData[value].split('~');
						break;
					case 'knowledgeNeeded':
						careerObj.preRequisite.knowledgeNeeded = careerData[value].split('~');
						break;
					case 'educationNeeded':
						careerObj.preRequisite.educationNeeded = careerData[value].split('~');
						break;
					case 'languagesNeeded':
						careerObj.preRequisite.languagesNeeded = careerData[value].split('~');
						break;
					case 'otherThingsNeeded':
						careerObj.preRequisite.others = careerData[value].split('~');
						break;
					case 'potentialTrainingProviders':
						careerObj.potentialTrainingProviders = careerData[value].split('~');
						break;
					case 'minInvestment':
						careerObj.investmentNeeded.minInvestment = careerData[value];
						break;
					case 'maxInvestment':
						careerObj.investmentNeeded.maxInvestment = careerData[value];
						break;
					case 'otherInvestment':
						careerObj.investmentNeeded.otherInvestment = careerData[value].split('~');
						break;
					case 'investmentExplanation':
						careerObj.investmentNeeded.investmentExplanation = careerData[value].split('~');
						break;
					case 'sourceOfFunds':
						careerObj.sourceOfFunds = careerData[value].split('~');
						break;
					case 'typeOfWork':
						careerObj.typeOfWork = careerData[value].split('~');
						break;
					case 'whereToStart':
						careerObj.whereToStart = careerData[value].split('~');
						break;
					case 'futureDemand':
						careerObj.futureDemand = careerData[value].split('~');
						break;
					case 'functionalExperts':
						careerObj.functionalExperts = careerData[value].split('~');
						break;
					case 'minDuration':
						careerObj.careerDuration.minDuration = careerData[value];
						break;
					case 'maxDuration':
						careerObj.careerDuration.maxDuration = careerData[value];
						break;
					case 'hiringCompanies':
						careerObj.hiringCompanies = careerData[value].split('~');
						break;
					case 'whereToFindJobs':
						careerObj.whereToFindJobs = careerData[value].split('~');
						break;
					case 'minIncome':
						careerObj.income.minIncome = careerData[value];
						break;
					case 'maxIncome':
						careerObj.income.maxIncome = careerData[value];
						break;
					case 'incomeExplanation':
						careerObj.income.incomeExplanation = careerData[value].split('~');
						break;
					case 'risksAssociated':
						careerObj.risksAssociated = careerData[value].split('~');
						break;
					case 'expectedCareerSurvival':
						careerObj.expectedCareerSurvival = careerData[value].split('~');
						break;
					case 'nextStepsToGrow':
						careerObj.nextStepsToGrow = careerData[value].split('~');
						break;
					case 'interestAreas':
						careerObj.interestAreas = careerData[value].split('~');
						break;
					case 'reviewsByExperts':
						careerObj.reviewsByExperts = careerData[value].split('~');
						break;
					case 'tipsToMakeMoney':
						careerObj.tipsToMakeMoney = careerData[value].split('~');
						break;
					case 'partTime':
						careerObj.workingHours.partTime = careerData[value];
						break;
					case 'fullTime':
						careerObj.workingHours.fullTime = careerData[value];
						break;
					case 'freeLancer':
						careerObj.workingHours.freeLancer = careerData[value];
						break;
					case 'workFromHome':
						careerObj.workingHours.workFromHome = careerData[value];
						break;
					case 'searchKeywords':
						careerObj.searchKeywords = careerData[value].split('~');
						break;
					case 'images':
						careerObj.images = careerData[value].split('~');
						break;
				}
			}));
			resolve(careerObj);
		}).catch(function(err){
			reject(err);
		});
	});
}

function setCarrerObj(careerData){
	return new Promise(function(resolve, reject){
		co(function*(){
			var careerObj = {};
			careerData = JSON.parse(JSON.stringify(careerData));	
			yield Promise.each(Object.keys(careerData), co.wrap(function*(value,key){
				switch(value) {
					case 'name':
						careerObj.name = careerData[value];
						break;
					case 'description':
						careerObj.description = careerData[value];
						break;
					case 'stream':
						careerObj.stream = careerData[value];
						break;
					case 'typeOfStream':
						careerObj.typeOfStream = careerData[value];
						break;
					case 'preRequisite': 
						{
							careerObj.skillsNeeded = careerData[value].skillsNeeded ? careerData[value].skillsNeeded.join('~') : '';
							careerObj.knowledgeNeeded = careerData[value].knowledgeNeeded ? careerData[value].knowledgeNeeded.join('~') : '';
							careerObj.educationNeeded = careerData[value].educationNeeded ? careerData[value].educationNeeded.join('~') : '';
							careerObj.languagesNeeded = careerData[value].languagesNeeded ? careerData[value].languagesNeeded.join('~') : '';
							careerObj.otherThingsNeeded = careerData[value].others ? careerData[value].others.join('~') : '';
							break;
							
						}
					case 'potentialTrainingProviders':
						careerObj.potentialTrainingProviders = careerData[value].join('~');
						break;
					case 'investmentNeeded':
						{
							careerObj.minInvestment = careerData[value].minInvestment ? careerData[value].minInvestment : '';
							careerObj.maxInvestment = careerData[value].maxInvestment ? careerData[value].maxInvestment : '';
							careerObj.otherInvestment = careerData[value].otherInvestment ? careerData[value].otherInvestment.join('~') : '';
							careerObj.investmentExplanation = careerData[value].investmentExplanation ? careerData[value].investmentExplanation.join('~') : '';
							break;							
						}
					case 'sourceOfFunds':
						careerObj.sourceOfFunds = careerData[value].join('~');
						break;
					case 'typeOfWork':
						careerObj.typeOfWork = careerData[value].join('~');
						break;
					case 'whereToStart':
						careerObj.whereToStart = careerData[value].join('~');
						break;
					case 'futureDemand':
						careerObj.futureDemand = careerData[value].join('~');
						break;
					case 'functionalExperts':
						careerObj.functionalExperts = careerData[value].join('~');
						break;
					case 'careerDuration':
						{
							careerObj.minDuration = careerData[value].minDuration ? careerData[value].minDuration :'';
							careerObj.maxDuration = careerData[value].maxDuration ? careerData[value].maxDuration :'';
							break;	
						}
					case 'hiringCompanies':
						careerObj.hiringCompanies = careerData[value].join('~');
						break;
					case 'whereToFindJobs':
						careerObj.whereToFindJobs = careerData[value].join('~');
						break;
					case 'income':
						{
							careerObj.minIncome = careerData[value].minIncome ? careerData[value].minIncome : '';
							careerObj.maxIncome = careerData[value].maxIncome ? careerData[value].maxIncome : '';
							careerObj.incomeExplanation = careerData[value].incomeExplanation ? careerData[value].incomeExplanation.join('~') : '';
							break;							
						}
					case 'risksAssociated':
						careerObj.risksAssociated = careerData[value].join('~');
						break;
					case 'expectedCareerSurvival':
						careerObj.expectedCareerSurvival = careerData[value].join('~');
						break;
					case 'nextStepsToGrow':
						careerObj.nextStepsToGrow = careerData[value].join('~');
						break;
					case 'interestAreas':
						careerObj.interestAreas = careerData[value].join('~');
						break;
					case 'reviewsByExperts':
						careerObj.reviewsByExperts = careerData[value].join('~');
						break;
					case 'tipsToMakeMoney':
						careerObj.tipsToMakeMoney = careerData[value].join('~');
						break;
					case 'workingHours':
						{
							careerObj.partTime = careerData[value].partTime;
							careerObj.fullTime = careerData[value].fullTime;
							careerObj.freeLancer = careerData[value].freeLancer;
							careerObj.workFromHome = careerData[value].workFromHome;
							break;							
						}
					case 'searchKeywords':
						careerObj.searchKeywords = careerData[value].join('~');
						break;
					case 'images':
						careerObj.images = careerData[value].join('~');
						break;
				}
			}));
			resolve(careerObj);
		}).catch(function(err){
			reject(err);
		});
	});
}