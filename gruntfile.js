module.exports = function(grunt) {

	var jsToConcat =[
		'<%= careerapp.client %>/dist/js/component.js',
		'<%= careerapp.client %>/vendors/intl-tel-input/build/js/utils.js',
		'<%= careerapp.client %>/vendors/angular-ui-carousel/dist/ui-carousel.min.js',
		'<%= careerapp.client %>/assets/javascripts/bootstrap.js',
		'<%= careerapp.client %>/assets/javascripts/jstz.min.js',
		'<%= careerapp.client %>/assets/javascripts/materialize.js',
		'<%= careerapp.client %>/app/**/*.js',
		'<%= careerapp.client %>/dist/js/templates.js',
	];

	//Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		careerapp: {
			// configurable paths
			client:'public',
			dist: 'dist'
		},

		less: {
			dev: {
				options: {
					sourceMap: true,
					dumpLineNumbers: 'comments',
					relativeUrls: true,
					plugins: [
							new (require('less-plugin-autoprefix'))({ browsers: ["last 2 versions"] })
					]
				},
				files: {
					'public/assets/css/bootstrap.css': 'public/assets/less/bootstrap.less',
				}
			},
		},

		html2js: {
		  options: {
		    base: 'careerapp',
		    module: 'careerappTemplates',
		  },
		  main: {
	      	src: ['public/app/views/**/*.html'],
	      	dest: '<%= careerapp.client %>/dist/js/templates.js'
	      },
		},

		bower_concat: {
	      	all: {
	        	dest: '<%= careerapp.client %>/dist/js/component.js',
	        	exclude: [
	        		'jquery',
	        		'angular',
	          	//'intl-tel-input'
	        	],
	        	bowerOptions: {
	          		relative: false
	        	}
	      	}
    	},

		concat: {
			options: {
		    separator: ';'
		  },
		  dist: {
		    src: jsToConcat,
		    dest: '<%= careerapp.client %>/dist/js/app.js'
		  },
		},
		concat_css: {
			all: {
				src: ['public/assets/css/bootstrap.css',
					  'public/vendors/ngToast/dist/ngToast.min.css',
					  'public/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
					  'public/vendors/intl-tel-input/build/css/intlTelInput.css',
					  'public/vendors/angular-loading-bar/build/loading-bar.min.css',
					  'public/vendors/angucomplete-alt/angucomplete-alt.css',
					  'public/vendors/angular-ui-carousel/dist/ui-carousel.min.css',
					  'public/assets/css/navbar-fixed-side.css',
					  'public/assets/css/materialize.css',
					  'public/assets/css/matirialicon.css',
					  'public/assets/css/app.css',
					  'public/assets/css/custom.css'],
				dest: 'public/dist/css/app.css'
			}
		},
		
		uglify: {
		  dist: {
		    files: {
		      '<%= careerapp.client %>/dist/js/app.min.js': [ '<%= careerapp.client %>/dist/js/app.js' ]
		    },
		    options: {
		      mangle: false
		    }
		  }
		},
		clean: {
			dist: {
				files: [
					{
						dot: true,
						src: [
							'.tmp',
							'<%= careerapp.dist %>/*',
							'!<%= careerapp.dist %>/.git*'
						]
					}
				]
			},
			server: '.tmp'
		},

		jshint: {
		  all: [ 'gruntfile.js', 'public/app.js','public/routes.js','public/app/services/*.js','public/app/controllers/**/*.js']
		},
		copy: {
		  main: {
		    expand: true,
		    cwd: 'public/assets/fonts',
		    src: '**',
		    dest: 'public/dist/fonts',
		  }
		}
	});

	// Load NPM tasks
	require('load-grunt-tasks')(grunt);
	grunt.loadNpmTasks('grunt-bower-concat');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-copy');
	
	// Making grunt default to force in order not to break the project.
	grunt.option('force', true);
	//css tasks
	grunt.registerTask('dev', ['less:dev','concat','concat_css']);

	// Create build file
	grunt.registerTask('build',[
		'jshint',
		'less:dev',
		'concat_css',
		'html2js:main',
		'bower_concat',
		'concat:dist',
		'uglify:dist',
		'copy'
	]);
};